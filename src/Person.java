//creation of a class named Person
public class Person {
	//declaration of the String and double variables
	public String name;
	//variables which are private
	private double personWeight;
	private double personHeight;
	
	//creation of an enum named weightUnits
	public enum weightUnits {
		KILOS,POUNDS
	}
	//Creation of another enum named heightUnits
	public enum heightUnits {
		CENTIMETERS, INCHES
	}
	
	//Creation of a method named convertUnits that has four parameters
	public void convertUnits(double pWeight, double pHeight, weightUnits wt, heightUnits ht) {
		
		//condition for checking KILOS and CENTIMETERS units using enum
		if(wt == weightUnits.KILOS && ht == heightUnits.CENTIMETERS ) {
			//making conversions
			this.personWeight = pWeight;
			this.personHeight = pHeight / 100;
		}
		
		//condition for checking POUNDS and INCHES units using enum
		else if(wt == weightUnits.POUNDS && ht == heightUnits.INCHES) {
			//making conversions
			this.personWeight = pWeight / 2.205;
			this.personHeight = (pHeight * 12) / 39.37;
		}
		else {
			//incase of invalid conversions
			System.out.println("INVALID OUTPUT");
		}
	}
	//a method named BMIgetter for calculating BMI 
	//and returning the required BMI
	public double BMIgetter() {
		//calculating the person's BMI
		double personsBMI = personWeight / (personHeight * personHeight);
		//returning the person's BMI
		return personsBMI;
	}
	
	//a method for showing the BMI info including the person's name
	public void displayBMIInfo() {
		System.out.println("Person's Name: " + name);
		System.out.println("BMI is: " + BMIgetter() + "\n");
	}
}