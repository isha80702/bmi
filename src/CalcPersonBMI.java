//Creating a simulation class named CalcPersonBMI
//and accessing the attributes via the enum and a method
public class CalcPersonBMI {
	public static void main(String args[]) {
		//creation of the class's instance named person1
		Person person1 = new Person();
		
		//creation of the class's instance named person2
		Person person2 = new Person();
		
		//setting the height 172 cm and weight 82 kg of Robin
		person1.name = "Raaj";
		person1.convertUnits(82, 172, Person.weightUnits.KILOS, Person.heightUnits.CENTIMETERS);
		
		//setting the height 5'10" and weight 152 pounds of Shreya
		person2.name = "Simran";
		person2.convertUnits(152, 5.10, Person.weightUnits.POUNDS, Person.heightUnits.INCHES);
		
		//Displaying the BMI of the Person's instance named Robin
		person1.displayBMIInfo();
		
		//Displaying the BMI of the Person's instance named Shreya
		person2.displayBMIInfo();
	}
}